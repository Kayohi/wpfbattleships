﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace BattleshipsGameLogic.Helpers
{
    public class PositionsComparer : IEqualityComparer<Position>
    {
        public bool Equals(Position p1, Position p2)
        {
            return p1.Column == p2.Column && p1.Row == p2.Row;
        }

        public int GetHashCode([DisallowNull] Position obj)
        {
            return obj.Row + obj.Column;
        }
    }
}
