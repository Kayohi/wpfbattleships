﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BattleshipsGameLogic.Ships;

namespace BattleshipsGameLogic
{
    public interface IHitHandler
    {
        /// <summary>
        /// handles hit performed by user
        /// </summary>
        /// <param name="shotPosition">position where user shoot</param>
        /// <returns>true if hit is positive</returns>
        bool HandleHit(Position shotPosition);
    }

    public class GameLogic : IHitHandler
    {
        /// <summary>
        /// 5 - Battleship
        /// 4 - Destroyer
        /// </summary>
        private readonly int[] possibleShipsRepresentedByTheirLength = { 5, 4, 4 };
        private readonly int mapSize;
        private readonly Action gameEndedCallback;
        List<Ship> availableShips;
        List<Position> shipPositions;

        public GameLogic(int mapSize, Action gameEndedCallback)
        {
            if (gameEndedCallback == null)
            {
                throw new ArgumentNullException("gameEndedCallback must not be null");
            }
            this.mapSize = mapSize;
            this.gameEndedCallback = gameEndedCallback;
            StartGame();
        }

        private void StartGame()
        {
            availableShips = new List<Ship>();
            shipPositions = new List<Position>();

            foreach (var shipLength in possibleShipsRepresentedByTheirLength)
            {
                ShipBuilder sb = new ShipBuilder(shipLength, mapSize, shipPositions);
                var newShip = sb.Build();
                availableShips.Add(newShip);
                shipPositions.AddRange(newShip.ResidingPositions);
            }
        }

        public bool HandleHit(Position shotPosition)
        {
            var isHit = this.shipPositions.FirstOrDefault(pos => pos.Column == shotPosition.Column && pos.Row == shotPosition.Row);
            if (isHit != null)
            {
                this.shipPositions.Remove(isHit);
                if (!this.shipPositions.Any())
                {
                    Task.Run(async () => { await Task.Delay(100); gameEndedCallback(); });
                }
                return true;
            }
            return false;
        }
    }
}
