﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BattleshipsGameLogic
{
    /// <summary>
    /// Represents cell board coordinate.
    /// 11-------x1
    /// |
    /// |
    /// |
    /// 1y
    /// </summary>
    public class Position
    {
        public Position(int column, int row)
        {
            this.Column = column;
            this.Row = row;
        }

        public int Column { get; set; }
        public int Row { get; set; }
    }
}
