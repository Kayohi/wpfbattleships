﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleshipsGameLogic.Ships
{
    public class ShipBuilder
    {
        private readonly int length;
        static Random r = new Random();
        private Ship.ShipOrientation orientation;
        private List<Position> positions;

        public ShipBuilder(int length, int mapSize, List<Position> takenPositions)
        {
            this.length = length;
            var random = r.Next(0, 1);
            //Heat up the random generator.
            for (int i = 0; i < 1000; i++)
            {
                random = r.Next(0, 2);
            }
            orientation = random == 0 ? Ship.ShipOrientation.Horizontal : Ship.ShipOrientation.Vertical;
            var increaseColumn = 1 - random;

            var topLeftCornerPosition = GenerateTopLeftCornerPositionBasedOnTakenPositions(takenPositions, length, orientation, mapSize);
            positions = new List<Position>();
            positions.Add(topLeftCornerPosition);
            for (int i = 1; i < length; i++)
            {
                positions.Add(new Position(topLeftCornerPosition.Column + i * increaseColumn, topLeftCornerPosition.Row + i * random));
            }
        }

        public static Position GenerateTopLeftCornerPositionBasedOnTakenPositions(List<Position> takenPositions, int wantedShipLength, Ship.ShipOrientation orientation, int mapSize)
        {
            var notPossiblePositions = new List<Position>();
            var incrementColumn = orientation == Ship.ShipOrientation.Horizontal ? 1 : 0;
            var incrementRow = 1 - incrementColumn;

            foreach (var taken in takenPositions)
            {
                for (var shipLength = wantedShipLength; shipLength >= 0; shipLength--)
                {
                    var notPossibleColumn = taken.Column - shipLength * incrementColumn;
                    var notPossibleRow = taken.Row - shipLength * incrementRow;
                    if (notPossibleColumn > 0 && notPossibleRow > 0)
                    {
                        notPossiblePositions.Add(new Position(notPossibleColumn, notPossibleRow));
                    }
                }
            }

            var possiblePositions = new List<Position>();
            var columnLimit = orientation == Ship.ShipOrientation.Horizontal ? mapSize - wantedShipLength + 1 : mapSize;
            var rowsLimit = orientation == Ship.ShipOrientation.Horizontal ? mapSize : mapSize - wantedShipLength + 1;

            for (int column = 1; column <= columnLimit; column++)
            {
                for (int row = 1; row <= rowsLimit; row++)
                {
                    possiblePositions.Add(new Position(column, row));
                }
            }
            var removedElementsCount = possiblePositions.RemoveAll(possible => notPossiblePositions.Any(notPossible => notPossible.Column == possible.Column && notPossible.Row == possible.Row));
            if (!possiblePositions.Any())
            {
                throw new Exception("Finding possible position is not possible");
            }
            int randomPositionIndex = r.Next(0, possiblePositions.Count);
            return possiblePositions[randomPositionIndex];
        }

        public Ship Build()
        {
            return new Ship() { Length = length, Orientation = orientation, ResidingPositions = positions.ToArray() };
        }
    }
}
