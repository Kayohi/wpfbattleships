﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BattleshipsGameLogic.Ships
{
    public class Ship
    {
        public enum ShipOrientation { Vertical, Horizontal};
        public ShipOrientation Orientation { get; set; }
        public int Length { get; set; }

        public Position[] ResidingPositions { get; set; }
    }
}
