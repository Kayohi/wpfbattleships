﻿using BattleshipsGameLogic;
using BattleshipsGameLogic.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BattleshipsUnitTestProject
{
    [TestClass]
    public class PositionsComparerTest
    {
        [TestMethod]
        public void ComparePositions_PositionsAreEqual()
        {
            var p1 = new Position(1, 1);
            var p2 = new Position(1, 1);

            var comparer = new PositionsComparer();
            var result = comparer.Equals(p1, p2);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void ComparePositions_PositionsAreDifferent()
        {
            var p1 = new Position(1, 1);
            var p2 = new Position(1, 2);
            var comparer = new PositionsComparer();
            var result = comparer.Equals(p1, p2);

            Assert.IsFalse(result);
        }
    }
}
