using BattleshipsGameLogic;
using BattleshipsGameLogic.Helpers;
using BattleshipsGameLogic.Ships;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace BattleshipsUnitTestProject
{
    [TestClass]
    public class ShipBuilderTest
    {

        [TestMethod]
        public void ProveShipGeneratorIsCorrect()
        {
            //Assign
            int trialCount = 1000000;
            int mapSize = 10;
            int[] possibleShipsRepresentedByTheirLength = { 5, 4, 4 };
            int sumOfAllTakenFields = possibleShipsRepresentedByTheirLength.Sum();

            for (int i = 0; i < trialCount; i++)
            {

                var availableShips = new List<Ship>();
                var shipPositions = new List<Position>();


                //Act
                foreach (var shipLength in possibleShipsRepresentedByTheirLength)
                {
                    ShipBuilder sb = new ShipBuilder(shipLength, mapSize, shipPositions);
                    var newShip = sb.Build();
                    availableShips.Add(newShip);
                    shipPositions.AddRange(newShip.ResidingPositions);
                }
                var countBeforeDistinct = shipPositions.Count();
                shipPositions.Distinct(new PositionsComparer());

                //Assert
                Assert.AreEqual(countBeforeDistinct, shipPositions.Count());
                Assert.AreEqual(sumOfAllTakenFields, shipPositions.Count());
            }

        }
    }
}
