﻿using BattleshipsGameLogic;
using BattleshipsGameLogic.Helpers;
using BattleshipsGameLogic.Ships;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace WpfBattleships.Map
{
    public class GameManager
    {
        private static string[] alphabet = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J" };
        private const int mapSize = 10;
        private const double cellSize = 60;

        public GameManager(Canvas canvas)
        {
            this.canvas = canvas;
            mapCells = new List<MapCell>();
        }

        private Canvas canvas { get; }
        private List<MapCell> mapCells;
        private GameLogic gameLogic;


        public void StartGame()
        {
            mapCells.Clear();
            canvas.Children.Clear();
            gameLogic = new GameLogic(mapSize, () => { MessageBox.Show("Victory!!!"); });
            for (int column = 1; column <= mapSize; column++)
            {
                for (int row = 1; row <= mapSize; row++)
                {
                    string positionId = alphabet[column - 1] + row.ToString();
                    var cell = new MapCell(positionId, new Position(column, row), cellSize, gameLogic);
                    mapCells.Add(cell);
                    canvas.Children.Add(cell);
                    Canvas.SetTop(cell, cellSize * (row - 1));
                    Canvas.SetLeft(cell, cellSize * (column - 1));
                }
            }
        }

        internal bool HandleHitByText(string text)
        {
            var matchedMapCell = mapCells.FirstOrDefault(c => c.PositionId == text);
            if (matchedMapCell != null)
            {
                matchedMapCell.Hit();
                return true;
            }
            return false;
        }
    }
}
