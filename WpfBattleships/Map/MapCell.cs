﻿using System;
using System.Windows.Controls;
using System.Windows.Media;
using BattleshipsGameLogic;
using BattleshipsGameLogic.Ships;
using Color = System.Windows.Media.Color;

namespace WpfBattleships.Map
{
    public class MapCell : Button
    {
        public Color HitColor = Color.FromRgb(210, 32, 66);
        public Color WaterColor = Color.FromRgb(48, 196, 201);
        public Color HiddenColor = Color.FromRgb(200, 200, 200);
        private bool hasBeenAlreadyHit;

        public MapCell(string positionId, Position position, double cellSize, IHitHandler hitHandler)
        {
            this.position = position;
            this.PositionId = positionId;
            CurrentFillColor = HiddenColor;
            this.CellSize = cellSize;
            this.hitHandler = hitHandler;
        }

        private readonly IHitHandler hitHandler;

        /// <summary>
        /// Cell position in the grid example A5 -- COLUMN|ROW
        /// ABC... Column
        /// 123... Row
        /// </summary>
        private string positionId;

        /// <summary>
        /// Position in numeric format
        /// </summary>
        private Position position;

        public string PositionId
        {
            get { return positionId; }
            private set { positionId = value; UpdateLabel(); }
        }

        private void UpdateLabel()
        {
            this.Content = new TextBlock { Text = positionId };
        }

        private Color currentColor;
        private Color CurrentFillColor
        {
            get { return currentColor; }
            set { currentColor = value; UpdateColor(); }
        }

        private void UpdateColor()
        {
            this.Background = new SolidColorBrush(CurrentFillColor);
        }

        internal void Hit()
        {
            OnClick();
        }

        private double cellSize;

        public double CellSize
        {
            get { return cellSize; }
            private set { cellSize = value; CellSizeUpdated(); }
        }

        private void CellSizeUpdated()
        {
            this.Height = CellSize;
            this.Width = CellSize;
        }

        protected override void OnClick()
        {
            if (!hasBeenAlreadyHit)
            {
                this.CurrentFillColor = hitHandler.HandleHit(this.position) ? this.HitColor : this.WaterColor;
                hasBeenAlreadyHit = true;
            }
        }
    }
}
