﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfBattleships.Map;

namespace WpfBattleships
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private GameManager mm;

        public MainWindow()
        {
            InitializeComponent();

            EventManager.RegisterClassHandler(typeof(TextBox),
            TextBox.KeyUpEvent,
            new System.Windows.Input.KeyEventHandler(TextBox_KeyUp));
        }

        /// <summary>
        /// Handle scenario when user pressed Enter after writing position.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key != System.Windows.Input.Key.Enter) return;

            e.Handled = true;
            HitButton_Click(sender, null);
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            mm = new GameManager(this.Canvas);
            mm.StartGame();
            StartButton.Content = "Restart";
            HitsProvidedByTextPanel.Visibility = Visibility.Visible;
            CustomPosition.Text = "";
        }

        private void ShowButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (var child in this.Canvas.Children)
            {
                var btn = child as Button;
                if (btn != null)
                {
                    ButtonAutomationPeer peer = new ButtonAutomationPeer(btn);
                    var invokeProv = peer.GetPattern(PatternInterface.Invoke) as IInvokeProvider;
                    invokeProv.Invoke();
                }
            }
        }

        private void HitButton_Click(object sender, RoutedEventArgs e)
        {
            if (!mm.HandleHitByText(CustomPosition.Text))
            {
                MessageBox.Show("Wrong position: " + CustomPosition.Text);
            }
            else
            {
                CustomPosition.Text = "";
            }
        }
    }
}
